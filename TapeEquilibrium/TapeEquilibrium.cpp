#include <cstdlib>
#include <numeric>

int sumL = 0, sumR = 0, total = 0, mini = 10000;
int difference = 0;
int solution(vector<int> &A) {
    if (A.size() == 2){
        return abs(A[0] - A[1]);
    }
    for (unsigned int i = 0; i < A.size(); i++){
        total += A[i];
    }
    for (unsigned int P = 0; P < A.size()-1; P++){
        sumL += A[P]; 
        sumR = total - sumL;
        difference = abs(sumR - sumL);
        if (difference < mini){
            mini = difference;
        }
    }
    return mini;
}