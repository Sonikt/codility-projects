#include <bits/stdc++.h>
int mini = 1;

int solution(vector<int> &A) {
    sort(A.begin(), A.end());    
    for(unsigned int i = 0; i < A.size(); i++){
        if(A[i] == mini){
            mini++;
        }
    }
    return mini;
}