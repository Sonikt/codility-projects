unsigned int add = 0;
unsigned long pairs = 0;
int solution(vector<int> &A) {
    // Implement your solution here
    for(unsigned int i = 0; i < A.size(); i++){
        if(A[i] == 0){
            add++;
        }else if(A[i] == 1){
            pairs += add;
        }
    }
    if (pairs > 1000000000){
        return -1;
    }
    return pairs;
}