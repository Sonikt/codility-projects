unsigned int pos = 1, prev_i = 0;
int solution(int X, vector<int> &A) {
    for(unsigned int i = 0; i <A.size(); i++){
        if(A[i] == pos && pos-1 < X){
            if(i > prev_i){
                prev_i = i;
            }
            if(pos == X ){
                return prev_i;
            }
            pos++;
            i = 0;
        }
    }
    if(pos -1 < X){
        return -1;
    }
}