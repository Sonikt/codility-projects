#include <cmath>
unsigned int remaining = 0, jumps = 0;

int solution(int X, int Y, int D) {
    // X - initial position
    // Y - minimal desired position
    // D - jump length
    remaining = Y-X ;
    jumps = remaining/D;
    if (jumps * D < remaining)
        jumps++;
    return jumps;
}
