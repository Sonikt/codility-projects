#include <iostream>
#include <vector>

using namespace std;

vector<int> solution(vector<int> &A, int K) {
    vector<int> a ;
    if (A.size() > 0)     
        a = A;
    int j = 0;
    int size = A.size();
    K = K%size;
    for(int i = 0; i < size; i++){
        j = (i+K)%size;
        a[j] = A[i];
    }
    return a;
}