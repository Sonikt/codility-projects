int gapSize = 0, sol = 0;
bool wasOne = false;

int solution(int N) {
    for(int i = 0; i < sizeof(N)*8; i++){
        if((N >> i) & 1){
            wasOne = true;
            if(gapSize > sol){
                sol = gapSize;
            }
            gapSize = 0;
        }else if(wasOne){
            gapSize++;
        }    
    }
    return sol;
}