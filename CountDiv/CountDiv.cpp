int solution(int A, int B, int K) {
    int increment = 0, decrement = 0, res = 0, num = 0;
        res = A%K;
        if(res > 0){
            if(K < A){
                increment = res;
            }else{
                increment = K - A;
            }
        }
        res = B%K;
        if(res > 0)decrement = res;

        if(A != B){
            num = ((B - decrement - A - increment)/K) + 1;
        }else if(A == B && A == 0){
            num = 1;
        }else if(A== B){
            if (A%K == 0) num = 1;
        }
    return num;
}