#include <algorithm>
using namespace std;

int solution(vector<int> &A) {
    sort(A.begin(),A.end());
    if(A[0] != 1){
        return false;
    }
    for(int i = 1; i < A.size(); i++){
        if(A[i] - A[i-1] != 1){
            return false;
        }
    }
    return true;
}