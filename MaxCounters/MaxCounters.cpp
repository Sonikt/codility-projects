#include <algorithm>

unsigned int maxi = 0;
vector<int> solution(int N, vector<int> &A) {
    vector<int> Aux(N);
    for(unsigned int i = 0; i < A.size(); i++){
        if(A[i] <= N){
            Aux[A[i]-1]++;
            if(Aux[A[i]-1] == maxi + 1){
                maxi = Aux[A[i]-1];
            }
        }else{
            fill(Aux.begin(), Aux.end(), maxi);
        }
    }
    return Aux;
}