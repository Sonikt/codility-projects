int solution(vector<int> &A) {
    unsigned long sum = 0;
    unsigned long N = A.size() + 1;
    unsigned long total = ((N+1)*N)/2;

    // Implement your solution here
    for (int i = 0; i< A.size(); i++){
        sum += A[i];
    }
    return total - sum;
}