int solution(vector<int> &A) {
    unsigned long long int odd = 0;
    
    for (unsigned int i=0; i < A.size(); i++) {
        // A xor B xor A = B
        odd ^= A[i];
    }
    
    return odd;
}

